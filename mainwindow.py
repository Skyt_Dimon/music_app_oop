from tkinter import *
from creational.singleton import Singleton
from windowsetings import eqswindow, renderwindow, samplewindow, strategywindow, footerwindow, decoratorwindow, \
    iteratorwindow, convertwindow


class Window(Tk, Singleton):
    def init(self):
        print('calling from init')
        super().__init__()

        self.button = Button(self, text='Open eqs window', command=self.create_window_eqs)
        self.button.pack(expand=True)

        self.button = Button(self, text='Open footer', command=self.create_footer_eqs)
        self.button.pack(expand=True)

        self.button = Button(self, text='Converter', command=self.create_convert)
        self.button.pack(expand=True)

        self.button = Button(self, text='Decorator', command=self.create_decorator)
        self.button.pack(expand=True)

        self.button = Button(self, text='Export', command=self.create_render)
        self.button.pack(expand=True)

        self.button = Button(self, text='Search', command=self.create_iterator)
        self.button.pack(expand=True)

        self.button = Button(self, text='Sampler', command=self.create_groups)
        self.button.pack(expand=True)

        self.button = Button(self, text='Strategy', command=self.create_strategy)
        self.button.pack(expand=True)
    def create_window_eqs(self):
        global extraWindow
        extraWindow = eqswindow.Extra()

    def create_footer_eqs(self):
        global extraWindow
        extraWindow = footerwindow.Extra()

    def create_convert(self):
        global extraWindow
        extraWindow = convertwindow.Extra()

    def create_decorator(self):
        global extraWindow
        extraWindow = decoratorwindow.Extra()

    def create_render(self):
        global extraWindow
        extraWindow = renderwindow.Extra()

    def create_iterator(self):
        global extraWindow
        extraWindow = iteratorwindow.Extra()

    def create_groups(self):
        global extraWindow
        extraWindow = samplewindow.Extra()

    def create_strategy(self):
        global extraWindow
        extraWindow = strategywindow.Extra()
    def __init__(self):
        print('calling from __init__')
