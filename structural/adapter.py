from abc import ABC, abstractmethod


class Audio(ABC):
    @abstractmethod
    def audiorecord(self):
        pass

class Midi(ABC):
    @abstractmethod
    def midiTrack(self):
        pass
class AudioTrack(Audio):
    def audiorecord(self):
        print("Audio is playing...")
class MidiTrack(Midi):
    def midiTrack(self):
        print("Midi is playing...")



class AudioToMidiAdapter(Audio):
    def __init__(self):
        self.midi = MidiTrack()

    def audiorecord(self):
        self.midi.midiTrack()